<?php

namespace App\Repository;

use App\Entity\SterfInfo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SterfInfo|null find($id, $lockMode = null, $lockVersion = null)
 * @method SterfInfo|null findOneBy(array $criteria, array $orderBy = null)
 * @method SterfInfo[]    findAll()
 * @method SterfInfo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SterfInfoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SterfInfo::class);
    }

    // /**
    //  * @return SterfInfo[] Returns an array of SterfInfo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SterfInfo
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
