<?php

namespace App\Repository;

use App\Entity\Sterfreden;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Sterfreden|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sterfreden|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sterfreden[]    findAll()
 * @method Sterfreden[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SterfredenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sterfreden::class);
    }

    // /**
    //  * @return Sterfreden[] Returns an array of Sterfreden objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Sterfreden
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
