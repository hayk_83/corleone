<?php

namespace Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class TestCommand extends Command
{ 

    protected static $defaultName = 'app:test-app';

    protected function configure()
    {
        $this->setDescription('Testing my Symfony Console Application')
            ->setHelp('This is a test command');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');
        $outputQ = new Question('Type anything you want: ');
        $typed = $helper->ask($input, $output, $outputQ);

        $output->writeln([
            'Test Command',
            '============',
            '',
            'You typed: ' . $typed,
            '',
        ]);
    }

}
