<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190930142330 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE persoon (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, vader_id INTEGER DEFAULT NULL, voornaam VARCHAR(128) NOT NULL, achternaam VARCHAR(128) NOT NULL, bijnaam VARCHAR(128) NOT NULL, geboorte_info_geboortedatum DATE NOT NULL, sterf_info_sterfdatum DATETIME DEFAULT NULL)');
        $this->addSql('CREATE INDEX IDX_D8419A4BB85DC16B ON persoon (vader_id)');
        $this->addSql('CREATE TABLE sterfreden (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, reden VARCHAR(128) NOT NULL)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE persoon');
        $this->addSql('DROP TABLE sterfreden');
    }
}
