<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
class GeboorteInfo
{

    /**
     * @ORM\Column(type="date")
     */
    private $geboortedatum;

    public function getGeboortedatum(): ?date
    {
        return $this->geboortedatum;
    }

    public function setGeboortedatum(date $geboortedatum): self
    {
        $this->geboortedatum = $geboortedatum;

        return $this;
    }

    public function isGeborenInJaar(int $jaar): bool
    {
        return $jaar == $this->getJaar();
    }

    private function getJaar(): int
    {
        return $this->geboortedatum->format('Y');
    }
}
