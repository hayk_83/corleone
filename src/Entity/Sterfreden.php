<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SterfredenRepository")
 */
class Sterfreden
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $reden;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SterfInfo", mappedBy="sterfreden")
     */
    private $sterfInfos;

    public function __construct()
    {
        $this->sterfInfos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReden(): ?string
    {
        return $this->reden;
    }

    public function setReden(string $reden): self
    {
        $this->reden = $reden;

        return $this;
    }

    public function isDoor(string $reden): bool
    {
        return $this->reden == $reden;
    }
}
