<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
class SterfInfo
{

    public const DATEFORMAT = 'm-d-Y';

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $sterfdatum;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sterfreden", inversedBy="sterfInfos")
     */
    private $sterfreden;

    public function getSterfdatum(): ?\DateTimeInterface
    {
        return $this->sterfdatum;
    }

    public function setSterfdatum(?\DateTimeInterface $sterfdatum): self
    {
        $this->sterfdatum = $sterfdatum;

        return $this;
    }

    public function getSterfreden(): ?Sterfreden
    {
        return $this->sterfreden;
    }

    public function setSterfreden(?Sterfreden $sterfreden): self
    {
        $this->sterfreden = $sterfreden;

        return $this;
    }

    public function isOverledenOp(DateTime $op)
    {
        if (!isset($this->sterfdatum)) {
            return false;
        }
        return $this->sterfdatum->format(self::DATEFORMAT) == $op->format(self::DATEFORMAT);
    }

    public function isOverledenDoor(string $reden): bool
    {
        if (!isset($this->sterfreden)) {
            return false;
        }
        return $this->sterfreden->isDoor($reden);
    }
}
