<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PersoonRepository")
 */
class Persoon
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $voornaam;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $achternaam;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $bijnaam;

    /**
     * @ORM\Embedded(class="App\Entity\GeboorteInfo")
     */
    private $geboorteInfo;

    /**
     * @ORM\Embedded(class="App\Entity\SterfInfo")
     */
    private $sterfInfo;

    public function __construct()
    {
        $this->kinderen = new ArrayCollection;
        $this->geboorteInfo = new GeboorteInfo;
        $this->sterfInfo = new SterfInfo;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Persoon", inversedBy="kinderen")
     */
    private $vader;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Persoon", mappedBy="vader")
     */
    private $kinderen;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVoornaam(): ?string
    {
        return $this->voornaam;
    }

    public function setVoornaam(string $voornaam): self
    {
        $this->voornaam = $voornaam;

        return $this;
    }

    public function getAchternaam(): ?string
    {
        return $this->achternaam;
    }

    public function setAchternaam(string $achternaam): self
    {
        $this->achternaam = $achternaam;

        return $this;
    }

    public function getBijnaam(): ?string
    {
        return $this->bijnaam;
    }

    public function setBijnaam(string $bijnaam): self
    {
        $this->bijnaam = $bijnaam;

        return $this;
    }

    public function getGeboorteInfo(): GeboorteInfo
    {
        return $this->geboorteInfo;
    }

    public function setGeboorteInfo(GeboorteInfo $geboorteInfo): self
    {
        $this->geboorteInfo = $geboorteInfo;

        return $this;
    }

    public function getSterfInfo(): SterfInfo
    {
        return $this->sterfInfo;
    }

    public function setSterfInfo(SterfInfo $sterfInfo): self
    {
        $this->sterfInfo = $sterfInfo;

        return $this;
    }

    public function getVader(): ?self
    {
        return $this->vader;
    }

    public function setVader(?self $vader): self
    {
        $this->vader = $vader;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getKinderen(): ArrayCollection
    {
        return $this->kinderen;
    }

    public function addKind(self $kind): self
    {
        if (!$this->kinderen->contains($kind)) {
            $this->kinderen[] = $kind;
            $kind->setVader($this);
        }

        return $this;
    }

    public function removeKind(self $kind): self
    {
        if ($this->kinderen->contains($kind)) {
            $this->kinderen->removeElement($kind);

            if ($kind->getVader() === $this) {
                $kind->setVader(null);
            }
        }

        return $this;
    }
    
    public function getAantalKinderen(): int
    {
        return $this->kinderen->count();
    }
    
    public function containsKinderen(?array $kinderen): bool
    {
        if ($this->kinderen->isEmpty() && ($kinderen == null || empty($kinderen))) {
            return true;
        }
        foreach ($kinderen as $kind) {
            if (!$this->kinderen->contains($kind)) {
                return false;
            }
        }
        return true;
    }

    public function hasBijnaam(?string $bijnaam): bool
    {
        if (empty($this->bijnaam) && empty($bijnaam)) {
            return true;
        }
        return $this->bijnaam === $bijnaam;
    }



    public function isGeborenInJaar(int $jaar): bool
    {
        return $this->geboorteInfo->isGeborenInJaar($jaar);
    }

    public function isOverledenOp(DateTime $sterfdatum): bool
    {
        return $this->sterfInfo->isOverledenOp($sterfdatum);
    }

    public function isOverledenDoor(string $reden): bool
    {
        return $this->sterfInfo->isOverledenDoor($reden);
    }
}
